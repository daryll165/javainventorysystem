-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2021 at 03:16 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `archive_products`
--

CREATE TABLE `archive_products` (
  `id` int(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `bought` int(255) NOT NULL,
  `sold` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customers_info`
--

CREATE TABLE `customers_info` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'new',
  `email` varchar(100) DEFAULT NULL,
  `current_orders` varchar(200) DEFAULT NULL,
  `shipping_address` varchar(300) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers_info`
--

INSERT INTO `customers_info` (`id`, `name`, `type`, `email`, `current_orders`, `shipping_address`, `avatar`) VALUES
(1, 'Nova', 'regular', 'nova@gmail.com', '5 items', 'Tangail', 'Customers/nova.png'),
(2, 'Tumpa', 'new', 'tumpa@gmail.com', '3 items', 'Narayanganj', 'Customers/tumpa.png'),
(3, 'Kona', 'new', 'kona@gmail.com', '10 items', 'Barisal', 'Customers/kona.png'),
(4, 'Christine Rubio', 'Regular', 'christine@gmail.com', '12 items', 'Nasipit Talamban', 'Customers/pro3.png'),
(5, 'Justine Ambrad', 'New', 'justine@gmail.com', '3 items', 'Talamban', 'Customers/capture.png'),
(6, 'Rubylyn Hernani', 'New', 'rubylyn@gmail.com', '4 items', 'Talamban', 'Customers/cute.png'),
(7, 'Jurick Baybayanon', 'New', 'jurick@gmail.com', '5 items', 'Talamaban', 'Customers/cutest.png'),
(8, 'Melchor Casipong\r\n', 'New', 'melchor@gmail.com', '7 items', 'Talamban', 'Customers/isogkakol.png'),
(9, 'Ericka Gwapa', 'New', 'ericka@gmail.com', '10 items', 'Medellin', 'Customers/ericka.png');

-- --------------------------------------------------------

--
-- Table structure for table `deactivate`
--

CREATE TABLE `deactivate` (
  `id` int(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `bought` int(11) NOT NULL DEFAULT 0,
  `sold` int(11) NOT NULL DEFAULT 0,
  `image` varchar(300) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `bought`, `sold`, `image`, `created_at`, `updated_at`) VALUES
(14, 'Shampoo', 100, 38, 'Uploads/shampoo-removebg-preview.png', '2021-03-27 23:13:25', '2021-03-27 23:13:25'),
(20, 'Moisturizer', 100, 34, 'Uploads/cream-removebg-preview.png', '2021-03-30 13:34:01', '2021-03-30 13:34:01'),
(22, 'Hand Wash', 500, 122, 'Uploads/hand-removebg-preview.png', '2021-03-30 13:34:45', '2021-03-30 13:34:45'),
(23, 'Milk Powder', 40, 9, 'Uploads/milk-removebg-preview.png', '2021-03-30 13:35:07', '2021-03-30 13:35:07'),
(24, 'Soap Bar', 700, 204, 'Uploads/soap-removebg-preview.png', '2021-03-30 13:38:01', '2021-03-30 13:38:01'),
(30, 'Sunscreen', 56, 11, 'Uploads/sun-removebg-preview.png', '2021-03-31 01:32:14', '2021-03-31 01:32:14'),
(31, 'Green Tea', 130, 50, 'Uploads/green-removebg-preview.png', '2021-03-31 01:43:42', '2021-04-08 11:43:42'),
(32, 'Glucose', 234, 43, 'Uploads/glucose-removebg-preview.png', '2021-03-31 01:45:21', '2021-03-31 01:45:21'),
(33, 'Body Lotion', 58, 11, 'Uploads/body-removebg-preview.png', '2021-04-04 23:08:38', '2021-04-04 23:08:38'),
(45, 'Tooth Paste', 130, 35, '', NULL, '2021-05-29 06:41:34'),
(48, 'Bulads', 500, 50, '', NULL, '2021-05-29 06:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(15, 'Dada', 'gwapo123'),
(16, 'Waelhi', 'nobody'),
(17, 'Dario', 'gwapo123'),
(18, 'Daryll', 'gwapo123');

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

CREATE TABLE `users_info` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `u_name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `avatar` varchar(100) DEFAULT NULL,
  `last_login_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`id`, `name`, `u_name`, `email`, `password`, `is_active`, `is_admin`, `avatar`, `last_login_time`, `created_at`) VALUES
(16, 'Admin', 'riya', 'manag@gmail.com', '26107860cea2897564f137e754482b5f', 1, 1, 'Users/images.png', '2021-05-10 19:12:08', '2021-05-10 11:55:17'),
(20, 'Admin Angelica', 'angelica_guapa', 'angelica@gmail.com', 'a0c0f2e61894407cd906b2a0d4f8b257', 1, 0, 'Users/2021-04-14.jpg', '2021-05-13 14:16:09', '2021-05-13 22:13:02'),
(21, 'dadadaryll', 'dada165', 'darylls@gmail.com', 'gwapodaryll', 1, 0, 'Users/dscf2617.jpg', '2021-05-22 08:11:20', '2021-05-14 14:11:31'),
(22, 'vice cutena ganda', 'vice', 'vice@gmail.com', '08bf88818659a03d4f2c1059425d03aa', 1, 0, 'Users/img_0298.jpg', '2021-05-14 10:37:28', '2021-05-14 18:30:38'),
(24, 'Juanses', 'dsadsad', 'juan@gmail.com', '165f61348b668c74dccf4e7c76835d89', 1, 0, NULL, '2021-05-22 05:56:43', '2021-05-16 21:59:16'),
(25, 'Juans', 'dawdad', 'papang2415@gmail.com', '165f61348b668c74dccf4e7c76835d89', 1, 0, NULL, '2021-05-25 07:30:02', '2021-05-16 22:00:20'),
(26, 'Dario', 'dario_gwapo', 'dario@gmail.com', 'Dev!nek0', 1, 0, NULL, '2021-05-20 07:50:01', '2021-05-20 15:50:01'),
(27, 'dsa', 'dsadsa', 'ddsad', 'dsad', 1, 0, NULL, '2021-05-20 14:22:15', '2021-05-20 22:22:15'),
(28, 'Christine', 'christineguapa', 'christine@gmail.com', 'ttinegwapa', 1, 0, NULL, '2021-05-22 07:53:58', '2021-05-20 22:34:58'),
(29, 'Jeric', 'jejeric', 'jeric@gmail.com', 'jericguapo', 1, 0, NULL, '2021-05-20 14:43:37', '2021-05-20 22:43:37'),
(30, 'Daniel Padilla', 'daniel', 'dp@gmail.com', 'password', 1, 0, NULL, '2021-05-23 01:42:57', '2021-05-23 09:42:57'),
(31, 'daniel padilla', 'daniel', 'daniel@gmail.com', 'aa47f8215c6f30a0dcdb2a36a9f4168e', 1, 0, NULL, '2021-05-23 02:33:47', '2021-05-23 10:08:49'),
(33, 'Dario Vildosola', 'dario_guapo', 'dario@gmail.com', '161ebd7d45089b3446ee4e0d86dbcf92', 1, 0, NULL, '2021-05-25 14:14:35', '2021-05-25 15:53:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `archive_products`
--
ALTER TABLE `archive_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers_info`
--
ALTER TABLE `customers_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deactivate`
--
ALTER TABLE `deactivate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_info`
--
ALTER TABLE `users_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `archive_products`
--
ALTER TABLE `archive_products`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customers_info`
--
ALTER TABLE `customers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `deactivate`
--
ALTER TABLE `deactivate`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users_info`
--
ALTER TABLE `users_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
